import web3Service from './web3Service'
const abi = require('../json/InvoiceTapABI.json')

const invoiceContractService = {

  init () {
    // console.log(web3Service.web3)
    // console.log(process.env.VUE_APP_INVOICE_TAP_CONTRACT_ADDRESS)
  },
  fetchBids(billId) {
    return new Promise((resolve, reject) => {
      let web3Obj = web3Service.web3;
      let owner = web3Service.accounts[0]
      let contract = new web3Obj.eth.Contract(abi, process.env.VUE_APP_INVOICE_TAP_CONTRACT_ADDRESS);

      contract.methods.getBidsOnInvoice(billId).call({ from: owner }).then(resp => {
        resolve(resp)
      })
      .catch(error => {
        reject(error)
      })
    })
  },
  fetchBid(bidId) {
    return new Promise((resolve, reject) => {
      let web3Obj = web3Service.web3;
      let owner = web3Service.accounts[0]
      let contract = new web3Obj.eth.Contract(abi, process.env.VUE_APP_INVOICE_TAP_CONTRACT_ADDRESS);

      contract.methods.showBid(bidId).call({ from: owner }).then(resp => {
        resolve(resp)
      })
      .catch(error => {
        reject(error)
      })
    })
  },
  fetchMyBid(billId) {
    return new Promise((resolve, reject) => {
      let web3Obj = web3Service.web3;
      let owner = web3Service.accounts[0]
      let contract = new web3Obj.eth.Contract(abi, process.env.VUE_APP_INVOICE_TAP_CONTRACT_ADDRESS);
      contract.methods.showBidOnInvoice(owner, billId).call({ from: owner }).then(resp => {
        resolve(resp)
      })
      .catch( error => {
        reject(error)
      })
    })
  },
  submitQuote(quote) {
    return new Promise((resolve, reject) => {
      let web3Obj = web3Service.web3;
      let owner = web3Service.accounts[0]
      let contract = new web3Obj.eth.Contract(abi, process.env.VUE_APP_INVOICE_TAP_CONTRACT_ADDRESS);
      contract.methods.bid(quote.billId, quote.amount).send({ from: owner }).then(reciept => {
        resolve(reciept)
      }).catch(error => {
        reject(error)
      })
    })
  },
  removeQuote(bolID) {
    return new Promise((resolve, reject) => {
      let web3Obj = web3Service.web3;
      let owner = web3Service.accounts[0]
      let contract = new web3Obj.eth.Contract(abi, process.env.VUE_APP_INVOICE_TAP_CONTRACT_ADDRESS);
      contract.methods.removeBid(bolID).send({ from: owner }).then(reciept => {
        resolve(reciept)
      }).catch(error => {
        reject(error)
      })
    })
  },
  submitBOL(bill) {
    return new Promise((resolve, reject) => {
      let web3Obj = web3Service.web3;
      let owner = web3Service.accounts[0]
      let contract = new web3Obj.eth.Contract(abi, process.env.VUE_APP_INVOICE_TAP_CONTRACT_ADDRESS);
      contract.methods.createInvoice(bill.bolID, bill.basePrice, bill.discount).send({ from: owner }).then(reciept => {
        resolve(reciept)
      }).catch(error => {
        reject(error)
      })
    })
  },
  userHasAlreadyBid(billId) {
    return new Promise ((resolve, reject) => {
      let web3Obj = web3Service.web3;
      let owner = web3Service.accounts[0]
      let contract = new web3Obj.eth.Contract(abi, process.env.VUE_APP_INVOICE_TAP_CONTRACT_ADDRESS);
      contract.methods.hasUserAlreadyBid(owner, billId).call({ from: owner }).then(resp => {
        resolve(resp)
      })
      .catch( error => {
        reject(error)
      })
    })
  },
  fetchInvoices() {
    return new Promise ((resolve, reject) => {
      let web3Obj = web3Service.web3;
      let owner = web3Service.accounts[0]
      let contract = new web3Obj.eth.Contract(abi, process.env.VUE_APP_INVOICE_TAP_CONTRACT_ADDRESS);
      contract.methods.getSubmittedInvoices().call({ from: owner }).then(resp => {
        resolve(resp)
      })
      .catch( error => {
        reject(error)
      })
    })
  },
  fetchTargetInvoice(invoiceID) {
    return new Promise ((resolve, reject) => {
      let web3Obj = web3Service.web3;
      let owner = web3Service.accounts[0]
      let contract = new web3Obj.eth.Contract(abi, process.env.VUE_APP_INVOICE_TAP_CONTRACT_ADDRESS);
      contract.methods.getSubmittedInvoice(invoiceID).call({ from: owner }).then(resp => {
        resolve(resp)
      })
      .catch( error => {
        reject(error)
      })
    })
  },
  isBOLAlreadySubmitted(bolID) {
    return new Promise ((resolve, reject) => {
      let web3Obj = web3Service.web3;
      let owner = web3Service.accounts[0]
      let contract = new web3Obj.eth.Contract(abi, process.env.VUE_APP_INVOICE_TAP_CONTRACT_ADDRESS);
      contract.methods.isBOLSubmitted(bolID).call({ from: owner }).then(resp => {
        resolve(resp)
      })
      .catch( error => {
        reject(error)
      })
    })
  }
}

invoiceContractService.init()
export default invoiceContractService