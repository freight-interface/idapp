const invoiceService = {
  calculateWeight: (invoice) => {
    let weights = invoice.items.map(item => item.weight)
    const reducer = (accumulator, currentValue) => accumulator + currentValue
    return weights.reduce(reducer)
  },

  calculateDistance: (invoice) => {
    return new Promise((resolve) => {
      if (google) {
        let service = new google.maps.DistanceMatrixService()
        service.getDistanceMatrix(
          {
            origins: [invoice.shipper.zipcode.toString()],
            destinations: [invoice.consignee.zipcode.toString()],
            travelMode: 'DRIVING'
          }, (res) => {
            let distance = 'N/A'
            if (res.rows.length && res.rows[0].elements.length && res.rows[0].elements[0].distance) {
              distance = res.rows[0].elements[0].distance.text
            }
            invoice.total_distance = distance
            resolve(invoice)
          }, err => {
            invoice.total_distance = 'N/A'
            resolve(invoice)
          });
      } else {
        invoice.total_distance = 'N/A'
        resolve(invoice)
      }
    })
  }
}

export default invoiceService